From codercom/enterprise-base:ubuntu

USER root
RUN apt-get update && apt install nodejs -y  && apt install npm -y
# Installing of awscli
RUN apt-get install awscli -y
# Install kubectl 
CMD sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl\
    && sudo chmod +x kubectl && sudo mkdir -p ~/.local/bin/kubectl \
    && sudo mv ./kubectl ~/.local/bin/kubectl 
# Install eksctl
CMD curl --silent --location "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp 
CMD sudo  mv /tmp/eksctl /usr/local/bin 

USER coder
